----
title: 什么是shell
----

### bash

一说到命令行，我们真正指的是shell.shell就是一个程序，它接受从键盘输入的命令，然后把命令传递给操作系统去执行。几乎所有的Linux发行版都提供一个名为bash的来自GNU项目的shell程序。“bash”是“Bourne Again SHell“的缩写，所指的是这样一个事实，bash是最初Unix上由p[Steve Bourne 史蒂夫伯恩](https://zh.wikipedia.org/wiki/%E5%8F%B2%E8%92%82%E5%A4%AB%C2%B7%E4%BC%AF%E6%81%A9)写成shell程序的sh的增强版。

所以shell就是一个从键盘接受命令，然后把命令传递给操作系统去执行的程序，bash是一种shell程序。

### 终端仿真器

当使用图形用户界面时，我们需要另一个和shell交互的叫做终端仿真器的程序，在菜单中它们可能被简单称为“terminal"基本上它们都完成相同的事情，让我们能访问shell.

### 启动终端

	wangxiaoyudeMacBook-Pro:~ wangxiaoyu$
	
这叫做shell提示符，无论何时当shell准备好了去接受输入时，它就会出现。不同的shell可能会以各种各样的面孔显示，它通常包括你的用户名@主机，紧接着当前的工作目录和一个美元符号💲
如果提示符的最后一个字符是"#"而不是“$"那么这个终端会话就有超级用户权限，这意味着，你要么是以root用户的身份登录，要么是选择的终端仿真器提供了超级用户权限。许多Linux发行版默认保存最后输入的500个命令，按下上箭头就会出现上次的命令。

### 复制和粘贴

虽然shell是和键盘打交道的，但是你可以在终端仿真器里使用鼠标。X窗口系统（是GUI工作的底层引擎）内建了一种机制，支持快速拷贝和站忒技巧。按下鼠标左键，沿着文本拖动鼠标高亮一些文本，那么这些文本就被拷贝到一个由X管理的缓冲区里面，然后按下鼠标中键，这些文本就被粘贴到光标所在的位置。

注意：不要在一个终端窗口里使用Ctrl-c和Ctrl-v快捷键来执行拷贝和粘贴。它们不起作用，对于shell来说这两个控制代码有着不同的含义，它们早于Microsoft Windows定义复制粘贴的的含义，许多年之前就赋予了不同的意义。

### 几个简单的基础命令

**date**显示系统当前时间和日期

	wangxiaoyudeMacBook-Pro:~ wangxiaoyu$ date
	2020年 1月31日 星期五 10时16分53秒 CST
	
**cal**显示当前月份的日历

		wangxiaoyudeMacBook-Pro:~ wangxiaoyu$ cal
		      一月 2020
		日 一 二 三 四 五 六
		          1  2  3  4
		 5  6  7  8  9 10 11
		12 13 14 15 16 17 18
		19 20 21 22 23 24 25
		26 27 28 29 30 31
		
**df**查看磁盘剩余空间的数量
	
		wangxiaoyudeMacBook-Pro:~ wangxiaoyu$ df
		Filesystem    512-blocks      Used Available Capacity iused      ifree %iused  Mounted on
		/dev/disk1s5   489620264  21466584 155803688    13%  484313 2447617007    0%   /
		devfs                684       684         0   100%    1187          0  100%   /dev
		/dev/disk1s1   489620264 308791640 155803688    67% 2740319 2445361001    0%   /System/Volumes/Data
		/dev/disk1s4   489620264   2099240 155803688     2%       2 2448101318    0%   /private/var/vm
		map auto_home          0         0         0   100%       0          0  100%   /System/Volumes/Data/home
		
**exit**可以通过关闭终端仿真器窗口，或者是在shell提示符下输入exit命令来终止一个终端会话。





