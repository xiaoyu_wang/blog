----
title: 不同样式风格的输出方式
----
## 不同样式风格的输出方式

* 嵌套输出 nested
* 展开输出 expanded
* 紧凑输出	compact
* 压缩输出	compressed

### 如何在编译sass的时候指定输出风格

* 命令行编译：  sass --watch style.scss:style.css --style compressed

其实就是在原来编译指令的后面加上--style输出风格，所以只要依据需求，把compact compressed这样的参数写在--style后面就行。
* gulpfile.js代码

	var gulp = require('gulp');
	var sass = require('gulp-sass')
	
	gulp.task('sass',function(){
		return gulp.src('./sass/**/*.scss')
		.pipe(sass({outputStyle: 'nested'}).on('error',sass.logError)).pipe(gulp.dest('./css'));
	});
	
就是在原来sass()中加一个outputStyle：编译风格就好了，tips： 在不指定风格的情况下，默认是嵌套输出。
