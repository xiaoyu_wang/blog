----
title: 事件
----

### 事件
js程序采用了异步事件驱动编程模型。在这种程序设计风格下，当文档，浏览器，元素，或者与之相关的对象发生某些有趣的事情时，web浏览器就会产生事件。例如，当web浏览器加载完文档，用户把鼠标指针移动到超链接上或者敲击键盘时候，web浏览器都会产生事件。如果 JavaScript 应用程序关注特定类型的事件，那么它可以注册当这类事件发生时要调用的一个或多个函数。请注意，这种风格并不只应用于 Web 编程，所有使用图形用户界面的应用程序都采用了它，它们静待某些事情发生（即，它们等待事件发生），然后它们响应。
请注意，事件本身并不是一个需要定义的技术名词。简而言之，事件就是 Web 浏览器通知应用程序发生了什么事情，这种在传统软件工程中被称为观察员模式。
### offsetWidth width clientWidth

width: css中设置的大小
clientWidth: width + padding
offsetWidth: width + padding + border 

documentElement属性返回文档的根节点
语法：
documentObject.documentElement

document.body   返回html dom中的body节点即 <body>
document.documentElement  返回html dom中的root节点  即<html>

### 应用场景
在获取scrollTop方面的差异
兼容方案：   var scrollTop = document.documentElement.scrollTop || docuent.body.scrollTop

### scrollTop
可以获取或者设置一个元素的内容垂直滚动的像素数。一个元素的scrollTop值是这个元素的顶部到它的最顶部可见内容的距离的度量。当一个元素的内容没有产生垂直方向的滚动条，那么它的scrollTop值是0.

### html事件处理程序

某个元素支持额每种事件，都可以使用一个与之相应的事件处理程序同名的html特性来指定。这个特性的值应该是能够执行的javascript代码。例如要在按钮被单击时执行一些JavaScript，可以像下面这样编写代码：

	<input type="button" value="Click Me" onclick="console.log('Clicked')">


### clientX 事件属性，是一个事件属性。返回当事件被触发时鼠标指针相对于浏览器页面（或者客户区）的水平坐标。客户区域指的是当前的窗口。
语法：   event.clientX

### scrollWidth clientWidth offsetWidth 的区别

说明：   scrollWidth: 对象实际内容的宽度（注意强调的是元素中的内容），会随着对象中内容超过可是区域后而变大。

clientWidth :对象内容的可是区域的宽度，不包括滚动条边线，除非对象元素自身的显示变大才会变大是元素自身的一个可见区域

offsetWidth：  对象的整体的实际宽度，跟clientWidth的区别仅仅是包括滚动条边线，同样是随着对象的显示大小的变化而变化。

### target

在事件处理程序内部，对象this始终等于currentTarget的值，而target则只包含事件的实际目标。如果直接将事件处理程序指定给了目标元素，则this currentTarget 和 target包含相同的值。

	var btn = document.getElementById('myBtn')
	btn.onclick = function(event) {
		console.log(event.currentTarget === this);  // true
		console.log(event.target === this) // true
	}
	
这个例子检测了currentTarget和target与this的值。由于click事件的目标是按钮，因此三个值是相同的，但是如果事件处理程序存在于按钮的父节点中，那么这些值是不相同的

	document.body.onclick = function(event) {
		console.log(event.currentTarget === document.body)// true
		console.log(this === document.body) // true
		console.log(event.target === document.getElementById('myBtn'))   // true
	}
当点击这个例子中的按钮时候，this和currentTarget都等于docuent.body,因为事件处理程序是注册到这个元素上的，然而，target元素却等于按钮元素，因为它是click事件真正的目标。由于按钮上并没有注册事件处理程序，结果 click 事件就冒泡到了 document.body，在那里事件才得到了处理。

事件对象的 eventPhase 属性，可以用来确定事件当前正位于事件流的哪个阶段。如果是在捕获阶段调用的事件处理程序，那么 eventPhase 等于 1；如果事件处理程序处于目标对象上，则 eventPhase 等于 2；如果是在冒泡阶段调用的事件处理程序，eventPhase 等于 3。这里要注意的是，尽管“处于目标”发生在冒泡阶段，但 eventPhase 仍然一直等于 2。来看下面的例子。

	var btn = document.getElementById("myBtn");
btn.onclick = function(event){
    console.log(event.eventPhase); // 2
};
document.body.addEventListener("click", function(event){
    console.log(event.eventPhase); // 1
}, true);
document.body.onclick = function(event){
    console.log(event.eventPhase); // 3
};

当单击这个例子中的按钮时，首先执行的事件处理程序是在捕获阶段触发的添加到 document.body 中的那一个，结果会弹出一个警告框显示表示 eventPhase 的 1。接着，会触发在按钮上注册的事件处理程序，此时的 eventPhase 值为 2。最后一个被触发的事件处理程序，是在冒泡阶段执行的添加到 document.body 上的那一个，显示 eventPhase 的值为 3。而当 eventPhase 等于 2 时，this、target 和 currentTarget 始终都是相等的

只有在事件处理程序执行期间，event对象才会存在，一旦事件处理程序执行完毕，event对象就会销毁



### React

		class Square extends React.Component {
	  render() {
	    return (
	      <button className="square" onClick={() => alert('click')}>
	        {this.props.value}
	      </button>
	    );
	  }
	}
Doing onClick={alert('click')} would alert immediately instead of when the button is clicked.

意思是说做事件绑定的时候如果直接写onClick={alert('click')}是不对的会在你没有点击之前直接就执行了。因为在{}中的是js是会立即执行的。onClick只是React中的一个标记，是React认识的标志，React在编译的时候肯定会搜集这些标记来翻译成浏览器中的click事件。

In JavaScript classes, you need to explicitly call super(); when defining the constructor of a subclass.

在定义一个子类的constructor的时候务必记得要调用super();

		class Square extends React.Component {
	  constructor(props) {
	    super(props);
	    this.state = {
	      value: null,
	    };
	  }
	
	  render() {
	    return (
	      <button className="square" onClick={() => alert('click')}>
	        {this.props.value}
	      </button>
	    );
	  }
	}
	
	
用state来代替props

			class Square extends React.Component {
	  constructor(props) {
	    super(props);
	    this.state = {
	      value: null,
	    };
	  }
	
	  render() {
	    return (
	      <button className="square" onClick={() => this.setState({value: 'X'})}>
	        {this.state.value}
	      </button>
	    );
	  }
	}
	
Whenever this.setState is called, an update to the component is scheduled, causing React to merge in the passed state update and rerender the component along with its descendants.

每当this.setState方法被调用的时候，react就会根据最新的state来进行一次更新。

When you want to aggregate data from multiple children or to have two child components communicate with each other, move the state upwards so that it lives in the parent component. The parent can then pass the state back down to the children via props, so that the child components are always in sync with each other and with the parent.

当想要两个字组件交流或者父亲组件做一个数据的统计的时候，要move the state upwards 就是将state传递到父组件中去。


Now we need to change what happens when a square is clicked. The Board component now stores which squares are filled, which means we need some way for Square to update the state of Board.
Since component state is considered private, we can’t update Board’s state directly from Square.

state是组件私有的，我们不能直接在另一个组件中改变其他组件的state

The usual pattern here is pass down a function from Board to Square that gets called when the square is clicked. 

通常的处理方式是父组件pass down 一个方法给子组件来调用


There are generally two ways for changing data. The first method is to mutate the data by directly changing the values of a variable. The second method is to replace the data with a new copy of the object that also includes desired changes.

有两种改变数据的方式：第一种是直接修改原始数据，第二种是创建一个新的备份。

第一种：直接修改

	var player = {score: 1,name:'xiaoyu'};
	player.score = 2;
	// 现在player = {score: 2,name: 'xiaoyu'};
第二种：创建备份

	var player = {score: 1, name: 'xiaoyu'};
	var newPlayer = Object.assign({},player,{score:2});
	现在没有改变原来的player对象而是创建了一个新的对象
	
虽然两种方式的最终结果是一样的，但是第二种方式可以帮助我们提高组件的性能。

### 好处1: Easier Undo/Redo and Time Travel
Immutability also makes some complex features much easier to implement. For example, further in this tutorial we will implement time travel between different stages of the game. Avoiding data mutations lets us keep a reference to older versions of the data, and switch between them if we need to.

更加方便的回退。我们可以保持对旧版本数据的引用，从而更加从容的在新旧状态之间来回切换。
### 好处2： Tracking Changes 跟踪变化
Determining if a mutated object has changed is complex because changes are made directly to the object. This then requires comparing the current object to a previous copy, traversing the entire object tree, and comparing each variable and value. This process can become increasingly complex.

Determining how an immutable object has changed is considerably easier. If the object being referenced is different from before, then the object has changed. That’s it.

如果采用直接在原来的数据对象中修改的方式，那么当发生一个改变的时候要前后对数据对象进行对比找到做了改变的地方，而采用方式二则容易判断多了 ，只要对象的引用改变了，那数据对象就改变了。如此简单。
### 好处3: Determining When to Re-render in React

The biggest benefit of immutability in React comes when you build simple pure components. Since immutable data can more easily determine if changes have been made, it also helps to determine when a component requires being re-rendered.
To learn more about shouldComponentUpdate() and how you can build pure components take a look at Optimizing Performance.

最大的好处就是，当你写的组件是符合‘单一指责’的组件的时候，不改变原始数据的方式可以更加方便的判断是否发生了数据的变化继而它也更加方便的来判断是否要进行从新渲染。


## Functional Components
功能组件
We’ve removed the constructor, and in fact, React supports a simpler syntax called functional components for component types like Square that only consist of a render method. Rather than define a class extending React.Component, simply write a function that takes props and returns what should be rendered.

React支持一种我们称为‘功能组件‘的组件。它只包含一个render渲染函数。并且写的时候不用很麻烦的定义一个类来继承React.Component而是简单的写一个带参数的function 然后返回需要被渲染（render）的东西就可以了。
改写之前的组件

		function Square(props) {
			return (
				<button className="square" onClick={props.onClick}>{props.value}</button>
			)
		}



You’ll need to change this.props to props both times it appears. Many components in your apps will be able to be written as functional components: these components tend to be easier to write and React will optimize them more in the future.

大多数的组件都可以写成功能组件，这种组件写起来更加的简单并且react以后会对功能组件的优化越来越好。

Note that onClick={props.onClick()} would not work because it would call props.onClick immediately instead of passing it down.

一定要注意这里的方法调用的写法，{}中的js代码会立即执行，我们需要的是将一个函数传递下去不是直接执行这个函数。

We’ll want the top-level Game component to be responsible for displaying the list of moves. So just as we pulled the state up before from Square into Board, let’s now pull it up again from Board into Game – so that we have all the information we need at the top level.

用game组件来做一个历史的步骤的记录，就像我们将state从square提到board中去一样我们再次将state从board中提到game中去。
First, set up the initial state for Game by adding a constructor to it:
那么首先就要初始化game到state状态（既然设计到要保存状态那就要写constructor函数了，这个可以做为一个判断标准诶）

Then change Board so that it takes squares via props and has its own onClick prop specified by Game, like the transformation we made for Square earlier.

然后就要改写board了，让它从game传递出来的到props中获得squares和onClick事件属性。
接下来要做的：

* Delete the constructor in Board.
* Replace this.state.squares[i] with this.props.squares[i] in Board’s renderSquare.
* Replace this.handleClick(i) with this.props.onClick(i) in Board’s renderSquare.

删除board中的构造函数
替换 this.state.squares[i] 为 this.props.squares[i]
替换this.handleClick(i) 为 this.props.onClick(i)

### the keys

When a key is added to the set, a component is created; when a key is removed, a component is destroyed. Keys tell React about the identity of each component, so that it can maintain the state across rerenders. 

It’s strongly recommended that you assign proper keys whenever you build dynamic lists.

在生成动态列表时候一定要确保分配正确的keys
Component keys don’t need to be globally unique, only unique relative to the immediate siblings.











