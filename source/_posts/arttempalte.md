----
title: arttempalte
----

# arttempalte

## 循环语法

### 1 标准语法

		{{each target}}
			{{$indext}} {{$value}}
		{{/each}}
	
### 2 原始语法

		<% for (var i=0;i<target.length;i++){ %>
				<%=i%>    <%=target[i]%>
		<%}%>
		
	target支持array与object的迭代，默认值是$data.
	$value与$index可以自定义：	{{each target val key}}
## 变量
### 标准语法
	{{set temp = data.sub.content}}
### 原始语法
	<% var temp = data.sub.content; %>
## 模板继承
### 标准语法
	{{extend './layout.art'}}
	{{block 'head'}}...{{/block}}
### 原始语法
	<% extend('./layout.art') %>
	<% block('head', function(){ %>
		...
	<% }) %>
模板继承允许我们构建一个包含站点中共同元素的基本模板骨架
## 子模板
### 标准语法
	{{include './header.art'}}
	{{include './header.art' data}}
### 原始语法
	<% include('./header.art') %>
	<% include('./header.art', data)%>
## 内置变量清单
* $data 传入模板的数据
* $imports 外部导入的变量以及全局变量
* print 字符串输出函数
* include 子模板载入函数
* extend 模板继承模板导入函数
* block 模板块声明函数

## 解析规则
template.defaults.rules
art-template可以自定义模板解析规则，默认配置了原始语法与标准语法
### 修改界定符
	//原始语法的界定符规则
	template.defaults.rules[0].test = /<%(#?)((?:==|=#|[=-])?)[ \t]*([\w\W]*?)[ \t]*(-?)%>/;
	template.defaults.rules[1].test = /{{([@#]?)[ \t]*(\/?)([\w\W]*?)[ \t]*}}/;
下面是一个正则表达式，我们只是改变了界定符，

	将<% %> 修改为<??>

	var rule = template.defaults.rules[0];
	rule.test = new RegExp(rule.test.source.replace('%','<\\\?'))
## 压缩页面
template.defaults.minimize	
art-template内建的压缩器可以压缩html js css 它在编译阶段运行，因此完全不影响渲染速度，并且能够加快网络传输。
### 开启压缩
		template.defaults.minimize = true;
	
	
	
