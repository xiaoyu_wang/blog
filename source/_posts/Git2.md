----
title: git
----

### Git

#### bug分支
软件开发中bug就像家常便饭一样，有了bug就需要修复，在git中由于分支是如此的强大，所以，每个bug都可以通过一个新的临时分支来修复，修复后，合并分支，然后将临时分支删除。当你接受到一个代号为001的bug的任务的时候，很自然的，你想创建一个分支issue-001来修复它，但是，当前你正在dev上进行的工作还没有提交，并不是不想提交，而是工作只进行到一半，还没法提交，预计完成还需要1天的时间但是，必须在2个小时之内修复，怎么办？幸好git提供了一个stash功能，可以把当前工作现场储藏起来，等以后恢复现场后继续工作。

	$ git stash
	saved working directory and index state WIP on dev:6224937 add merge
	HEAD is now at 6224937 add merge
现在，用git status查看工作区，就是干净的（除非有没有被git管理的文件），因此可以放心的创建分支来修复bug，首先要确定在哪个分支上修复bug，假定需要在master分支上修复，就从master创建临时分支

	$ git chekcout master
	Switched to branch 'master'
	Your branch is ahead of 'origin/master' by 6 commits
	$ git checkout -b issue-01
	Switched to a now branch 'issue001'
现在修复bug，修复完后提交。修复完后，切换到master分支并完成合并，最后删除issue-001分支。OK现在是时候接着回到dev分支干活了。

	$ git checkout dev
	Switched to branch 'dev'
此时查看分支的话，会发现工作区是干净的，刚才的工作现成存到哪里去了？可以用git stash list命令看看

	$ git stash list
	stash@{0}:WIP on dev:6224937 add merge
工作现场还在，git把stash内容存在某个地方了，但是需要恢复一下，有两个办法：一个是用git stash apply恢复，但是恢复后，stash内容并不删除，你需要用git stash drop来删除