----
title: javascript语言精粹
----

### javascript语言精粹
#### 第一章

##### 语句

在web浏览器中每个 

	<script> 
	
标签提供一个被编译且立即执行的编译单元，JavaScript把它们一起抛到一个公共的全局名字空间中。
switch、while、for和do语句允许有一个可选的前置标签label。它配合break语句来使用。
for-in循环用于枚举一个对象的所有属性名。在每次循环中object的下一个属性名字符串被赋值给variale。通常需要使用object.hasOwnProperty(variable)这个方法来确定一个属性是否是该对象的成员，还是来自于原型链。

	for(myvar in obj){
		if(obj.hasOwnProperty(myvar)){
			//do something
		}
	}
do语句就像while语句，唯一的区别是它在代码执行之后而不是之前检测表达式的值，这就意味着代码块至少执行一次。
try语句执行一个代码块，并捕获该代码块抛出的任何异常。catch从句定义了一个新的变量variable来接受抛出的异常对象。
throw语句抛出一个异常，如果throw语句在一个try代码块中，那么控制流就会跳转到catch从句中。如果throw语句在函数中，则该函数调用被放弃，控制流跳转到调用该函数的try语句的catch从句中。throw语句中的表达式通常是一个对象字面量，包含一个name属性和一个message属性。异常捕获器可以使用这些信息去决定该做什么。return语句会导致从函数中提前返回，它也可以指定要被返回的值，如果没有指定返回的表达式，那么返回值就是undefined。JavaScript不允许在return关键字和表达式之间换行。
#### 字面量
对象字面量是一种可以方便的按照指定规格创建新对象的表示法，属性名可以是标示符或者字符串，这些名字被当做字面量而不是变量名来对待，所以对象的属性名在编译时才能知道。属性的值就是表达式。

### 第三章 对象
JavaScript的简单数据类型包括数字 字符串 布尔值 null值和undefined值。其他所有值都是对象。数字字符串和布尔值‘貌似’对象，因为它们有方法，但是它们是不可变的。JavaScript中的对象是可变的键控集合。在JavaScript中数组是对象，函数是对象正则表达式是对象。对象是属性的容器，其中每个属性都拥有名字和值，属性的名字可以是包括空字符串在内的任意字符串。属性值可以是除了undefined值之外的任何值。
JavaScript包含一种原型链的特性，允许对象继承另一个对象的属性。正确的使用它能减少对象初始化时消耗的时间和内存。
#### 检索
要检索对象里包含的值，可以采用在[]后缀中括住一个字符串表达式的方式。如果字符串表达式是一个字符串字面量，而且是一个合法的JavaScript标识符不是保留字，那么也可以使用.表示法来代替，优先考虑使用.

||  运算符可以用来填充默认值；

	var middle = stooge['middle-name'] || 'none',
	var status = flight.status || 'unknow'
尝试从undefined的成员属性中取值将会导致TypeError异常。这时可以使用&&运算符来避免错误。

	flight.e     // undefined
	flight.e.f   // throw "TypeError"
	flight.e && flight.e.f // undefined
	
#### 更新
对象里的值可以通过赋值语句来更新，如果属性名已经存在在对象里了，哪儿这个属性的值就会被替换，如果对象之前没有那个属性名，那么该属性就会被扩充到对象中。
#### 原型
每个对象都链接到一个原型对象，并且可以从中继承属性。所有通过对象字面量创建的对象都连接到Object.prototype，他是一个JavaScript中的标配对象。原型链在更新时是不起作用`的`，当我们对某个对象做出改变时，不会触及该对象的原型。原型链关系是一中动态的关系，如果我们添加一个新的属性到原型中，那么这个属性会立即对所有基于该原型的对象可见。
#### 删除
delete运算符可以用来删除对象的属性，如果对象包含该属性，那么该属性就会被移除。它不会触及原型链中的任何对象。
### 函数
函数对象链接到Function.proptotype。（Function.proptotype再链接到Object.proptotype）每个函数在创建时候会附加两个隐藏属性：1函数的上下文和实现函数行为的代码。每个函数在创建时也随配有一个prototype属性。它的值是一个拥有constructor属性且值为该函数的对象，这个和隐藏链接到Function.prototype完全不同。
函数字面量可以出现在任何允许表达式出现的地方。函数也可以被定义在其他函数中，一个内部函数除了可以访问自己的参数和变量，同时它也可以自由访问把它嵌套在其中的父函数的参数和变量。通过函数字面量创建的函数对象包含一个链接到外部上下文的链接。这被称为闭包。他是JavaScript强大表现力的来源。
JavaScript创建一个函数的时候，会给该对象设置一个‘调用’属性，当JavaScript调用一个函数时，可以理解为调用此函数的调用属性。

### 函数字面量
//创建一个名为add的变量，并用来把两个数字相加的函数赋值给它。

	var add = function(a,b){
		return a + b
	}
函数字面量包括四个部分：第一个部分是保留字function第二个部分是函数名（可以被省略），函数可以通过函数名来递归的调用自己。第三部分是包围在圆括号中的一组参数。多个参数用逗号隔开。第四部分是包围在花括号中的一组语句。这些语句是函数的主体，在函数被调用的时候执行。