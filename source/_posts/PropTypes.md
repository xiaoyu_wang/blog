----
title: PropTypes
----

### PropTypes

在React15.5版本之后，React的校验工作转移到了一个单独的库。
使用的时候安装prop-types库，它会导出一系列的验证器，用于确保收到的数据是有效的。

有如下不同的验证器：

	import PropTypes from 'prop-types';
	
	MyComponents.propTypes = {
	// 你可以指定某一个prop是一个特定的js类型，默认情况下这些都是可选的
	  optionalArray: PropTypes.array,
	  optionalBool: PropTypes.bool,
	  optionalFunc: PropTypes.func,
	  optionalNumber: PropTypes.number,
	  optionalObject: PropTypes.object,
	  optionalString: PropTypes.string,
	  optionalSymbol: PropTypes.symbol,
	  
	  optionalNode: PropTypes.node, // 该类型的prop代表的是一个节点
	  optionalElement: PropTypes.element,  // 代表一个React element 
	  optionalMessage: PropTypes.instanceOf(Message), // 声明这是一个类的实例
	  
	  optionalEnum: PropTypes.oneOf(['News', 'Photos']), // 保证值在某一个确定的范围内
	  
    optionalUnion: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
    PropTypes.instanceOf(Message)
  	]),         			// 保证是其中的一种类型的一个对象
  	optionalArrayOf: PropTypes.arrayOf(PropTypes.number),  // 某种特定类型的数组
	  	optionalObjectOf: PropTypes.objectOf(PropTypes.number),// 具有某种特定类型属性的对象
	  	optionalObjectWithShape: PropTypes.shape({
    color: PropTypes.string,
    fontSize: PropTypes.number
	  }),      // 直接指定长成什么样子的对象
	 requiredFunc: PropTypes.func.isRequired,    // 可以在后面追加isRequired 来确保提供指定类型的prop（如果没有指定就会发出警告）
	 requiredAny: PropTypes.any.isRequired,    // 代表任意类型的数据（但是必须要有）
	 
	 // 下面是一个自定义验证器
	 customProp: function(props, propName, componentName) {
    if (!/matchme/.test(props[propName])) {
      return new Error(
        'Invalid prop `' + propName + '` supplied to' +
        ' `' + componentName + '`. Validation failed.'
      );
    }
  	},
	  	customArrayProp: PropTypes.arrayOf(function(propValue, key, componentName, location, propFullName) {
	    if (!/matchme/.test(propValue[key])) {
	      return new Error(
	        'Invalid prop `' + propFullName + '` supplied to' +
	        ' `' + componentName + '`. Validation failed.'
	      );
	    }
	  })
	};
	}
	
	
	
### 可以指定默认的属性

	class Greeting extends React.Component {
	  render() {
	    return (
	      <h1>Hello, {this.props.name}</h1>
	    );
	  }
	}
	
		// Specifies the default values for props:
	Greeting.defaultProps = {
	  name: 'Stranger'
	};
	
	// Renders "Hello, Stranger":
	ReactDOM.render(
	  <Greeting />,
	  document.getElementById('example')
	);
	
	
	默认属性可以保证当组件不能从父组件获取属性的时候，可以有一个默认的属性，然后我们配置的属性验证器照样会对默认属性做检查。