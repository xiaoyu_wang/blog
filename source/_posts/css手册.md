----
title: css手册
----

## css手册

### position
sticky: css3中新增的定位属性。就像是relative和fixed的合体，当在屏幕中时按照常规流排版，当卷到屏幕外时则表现如fixed。该属性的表现就是现实中见到的吸附效果。
