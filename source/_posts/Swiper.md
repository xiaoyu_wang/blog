----
title: swiper
----

## Swiper
### initialSlide
设定初始化时候slide的索引
默认0

	<script>
		var ms = new Swiper('.sc',{
			initialSlide: 2,
		})
	</script>
### autoplay
自动切换的时间间隔
### autoplayDisableOnInteraction
用户操作swiper之后，是否禁止autoplay。默认为true：停止。
### autoplayStopOnLast
如果设置为true，当切换到最后一个slide时停止自动切换。（loop模式下无效）。
### hashnav
如需为每个slide增加散列导航（有点像锚链接）。将hashnav设置为true，并在每个slide处增加data-hash属性。
这样当你的swiper切换时你的页面url就会加上这个属性的值了，你也可以通过进入页面时修改页面url让swiper在初始化时切换到指定的slide。