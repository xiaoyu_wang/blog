----
title: https? ssl/tls
----

### 什么是安全？怎样才安全？

* 机密性
* 完整性
* 身份认证
* 不可否认

https除了协议名和端口号（443）这两点不同之外在语法、语义上和http完全一样。

https能够鉴别危险的网站，并且尽最大可能保证你的上网安全，防御黑客对信息的窃听、篡改或者“钓鱼”、伪造。

https的秘密在于它把层的传输协议由tcp/ip换成了ssl/tls，由Http over tcp/ip变成了http over ssl/tls,让http运行在了安全的ssl/tls协议上，收发报文不再使用Socket API,而是调用专门的安全接口。

### ssl/tls

ssl---安全套接层，在osi模型中处于第五层，由网景公司于1994年发明，有v2和v3两个版本，而v1版本因为有严重的缺陷从未公开过。

ssl发展到v3时期已经证明了它自身是一个非常好的安全通信协议，于是互联网工程组IETF在1999年把它改名为tls（传输层安全），正式标准化，版本号重新从1.0算起，所以其实TLS1.0就是SSLv3.1。

今天的tls已经发行了有三个版本，2006年的1.1、 2008年的1.2、和2018年的1.3,每个新版本都紧跟着密码学的发展和互联网的现状，持续强化安全和性能，已经成为了信息安全领域的权威标准。

目前应用最为广泛的tls是1.2，而之前的协议都已经被认为是不安全的，各大浏览器即将在2020年左右停止支持。

tls由记录协议、握手协议、警告协议、变更密码规范协议、扩展协议等几个子协议组成，综合使用了对称加密、非对称加密、身份认证等许多密码学前沿技术。

浏览器和服务器在使用tls建立连接的时候需要选择一组恰当的加密算法来实现安全通信，这些算法的组合被称为密码套件。

##### OpenSSL

是一个著名的开源密码学程序库和工具包，几乎支持所有公开的加密算法和协议，已经成为了事实上的标准，许多应用软件都会使用它来作为底层库来实现tls功能，包括常用的web服务器Apache、Nginx等。

