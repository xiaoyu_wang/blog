----
title: www
----

### www
www是Internet的多媒体信息查询工具。
Nginx是一个高性能的http和反向代理服务器，也是一个imp/pop3/smtp代理服务器。

由内核和模块组成，其中内核的设计非常的简单和微小，仅仅通过配置文件将客户端请求映射到一个location block而在这个location中所配置的。

Nginx后端反向代理能力强。占用内存很低。

后端有健康检查功能。
支持phpcgi  fastcgi

官方测试并发能力每秒5万。

代码简洁容易上手

#### 模块

* 核心模块： http模块 Event模块 mail模块
* 基础模块: http Access 模块  http fastCGI 模块  http Proxy模块 http Rewrite模块
* 第三方模块： http Upstream Request Hash 模块 Notice模块 和http Access Key模块。

Nginx的高并发得益于其采用了epoll模型，与传统的服务器程序架构不同 epoll模型是Linux内核2.6以后才出现的。


#### 安装

安装pcre正则支持

		yum install pcre-devel -y
		
解压

		tar xzf nginx-1.6.2.tar.gz

修改服务器版本号

		cd nginx-1.6.2;
		sed -i -e's/1.6.2//g' -e's/nginx\//WS/g' -e's/"NGINX"/"WS"/g' src/core/nginx.h
		
		
make 

make install


ps -ef | grep nginx     //查看是否启动


