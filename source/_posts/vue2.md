----
title: vue
----

## 介绍
Vue的核心库仅仅关注视图层。
像Vue这种跟artTemplate的不同之处就在于，artTemplate这一类都属于字符串模板引擎（技术）而Vue属于是DOM模板引擎技术。Vue官网的第一个例子就已经很好的解释了：	

	<div id='app'>
		{{message}}
	</div>
	
	var app = new Vue({
		el: '#app',
		data: {
			message: 'hello world'
		}
	})
	
	浏览器渲染结果：   hello world

这看起来跟单单渲染一个字符串很类似，但是vue在背后做了大量的工作，现在数据已经和DOM绑定在一起了。所有元素都是响应式的了。
## 数据与方法
当一个Vue实例被创建时，它向Vue的响应式系统中加入了其data对象中可以找到的所有的属性。当这些属性的值发生改变时，视图将会产生'响应'，即匹更新为新的值。
除了data属性，Vue实例暴露了一些有用的实例属性和方法。它们都有前缀$以便于用户自定义的属性作区分。例如

		var data = {a: 1}
		var vm = new Vue({
			el: '#exampel',
			data: data
		})
		
		vm.$data ==== data    // true
		vm.$el === document.getElementById('example') // true
		
		vm.$watch('a', function(newValue, oldValue){
			// 这个回调函数将会在vm.a改变后调用
		})
		
### 实例生命周期
一个实例在created了之后只是刚刚初始化了实例内部的this指针的指向
## 注意：
不要在选项属性或者回调函数上使用箭头函数，因为箭头函数是和父级上下文绑定在一起的，this不会是如你所预期的Veu实例，经常会导致错误的发生。
## 模板语法
Vue.js 使用了基于 HTML 的模板语法，允许开发者声明式地将 DOM 绑定至底层 Vue 实例的数据。所有 Vue.js 的模板都是合法的 HTML ，所以能被遵循规范的浏览器和 HTML 解析器解析	
在底层实现上，Vue将模板编译成虚拟DOM渲染函数。结合响应系统，在应用状态改变时，Vue能够智能地计算出重新渲染组件的最小代价并应用到DOM操作上。
### DOM模板解析注意事项
当使用DOM作为模板时，会受到HTML本身的一些限制，因为VUE只有在浏览器解析、规范化模板之后才能获取其内容。（这一句话要重点理解!）
## 自定义事件系统
我们知道，父组件使用PROP传递数据给子组件。但是子组件怎么跟父组件通信呢？这个时候就用到了Vue的自定义事件系统。
### 使用v-on绑定自定义事件
每个Vue实例都实现了事件接口：
* 使用$on(eventName)监听事件
* 使用$emit(eventName)触发事件

注意： Vue的事件系统与浏览器的EventTarget API有所不同。尽管它们运行起来类似，但是$on $emit并不是addEventListener    dispachEvent的别名。
另外，父组件可以在使用子组件的地方直接使用v-on来监听子组件触发的事件。
注意：  不能使用$on侦听子组件释放的事件，而必须在模板里面直接使用v-on绑定。
### 给组件绑定原生事件
有时候，可能我们想在某个组件的根元素上监听一个原生事件，可以使用v-on的修饰符.native 例如：

		<my-component v-on:click.native="doSomeThing"></my-component>
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		