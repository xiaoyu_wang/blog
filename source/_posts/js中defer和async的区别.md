----
title: js中defer和async的区别
----

## js中defer和async的区别
	//最原始的情况
	 <script src='script.js'></script>
	 没有defer或者async，浏览器会立即加载并执行指定的脚本，‘立即’的意思是指在渲染*script*标签之后的文档元素之前，也就是不等待后续载入的文档元素，读到就加载并执行。
	 //async的情况
	 <script asycn src='script.js'></script>
	 有了async,加载和渲染后续文档元素的过程将和script.js的加载与执行并行进行（异步）。
	 //defer的情况
	 <script defer src='script.js'></script>
	 有defer,加载后续文档元素的过程将和script.js并行进行（异步），但是script.js的执行要在所有元素解析完成之后，也就是DOMContentLoaded事件触发之前完成。是的，是之前。
	 
下面是一张三种加载方式的对比图
![Aaron Swartz](http://segmentfault.com/img/bVcQV0)

async和defer一样，不会阻塞当前文档的解析，他会异步下载脚本，但是和defer不同的是，asnync会在脚本下载完成后立即执行，如果项目中脚本之间存在依赖关系，不推荐使用async


### 补充说明：
DOMContentLoaded与OnLoad事件，前者：页面document已经解析完成，页面中的dom元素已经可用，但是页面中引用的图片、subframe可能还没加载完。后者Onload：页面中的所有资源都加载完毕，包括图片。浏览器的载入进度在这时才会停止。这两个时间点将页面加载的timeline分成了三个阶段。

### defer 属性声明
其实浏览器默认加载js文件是同步阻塞的就是因为怕js文件中会有比如document.write()或者类似的dom修改，而声明了defer属性的script标签，其实就是表明，这个脚本中不会有document.write或者其他dom修改。
### 延迟加载
什么是延迟加载？	

延迟加载: 有些js代码并不是页面初始化的时候就立刻需要的，而稍后的某些情况才需要的。延迟加载就是一开始并不加载这些暂时用不到的js,而是在需要的时候或者稍后再通过js的控制来异步加载。也就是将js切分成很多模块，页面初始化时只加载需要立即执行的js，然后其他js的加载延迟到第一次需要用到的时候再加载。特别是页面有大量不同的模块组成，很多可能暂时不用或者根本就没用到。就像*图片的延迟加载*在图片出现在可视区域时，才加载显示图片。

### script的两个阶段： 加载和执行

js的加载其实是由两个阶段组成： *下载内容（download bytes）* 和*执行（parse and execute）* 浏览器在下载完js的内容后就会立即对其解释执行，不管是同步的还是异步的。下载后都会立即执行，而浏览器在解析执行js阶段是阻塞任何操作的，这时候的浏览器处于无响应状态。我们都知道通过网络下载script需要明显的时间，单容易忽略第二阶段，解析和执行也是需要时间的，script的解析和执行所花的时间比我们想象中的要多。延迟执行的原理: 将js先异步下载缓存起来，但是不立即执行，而是在第一次需要的时候执行一次。利用特殊的技巧可以做到下载和执行的分离。比如将js的内容作为Image或者object对象加载缓存起来，这样就不会立即执行了，然后在第一次需要的时候再执行。

### script标签使用的历史

1script放在head中

	<head>
		<script src='..'></script>
	</head>
	
阻止了后续的下载，在IE6-7中script是顺序下载的，而不是现在的并行下载、顺序执行的方式，在下载和执行阶组织渲染，这就是页面卡的根源！

2 script放在页面底部

	<script src='..'></script>
	</body>
不阻止其他下载（底部/body前）
在IE6-7中script是顺序下载的；在下载和解析执行阶段阻止渲染

3 异步加载script

	var se = document.createElement('script')
	se.src = 'http://script.js'
	document.getElementsByTagName('head')[0].appendChild(se)
在所有浏览器中script都是并行下载，只在解析执行阶段阻止渲染

4 异步下载 + 按需执行（图片预加载）

	var se = new Image()
	se.onload = registerScript()
	se.src = 'http://anydomain.com/a.js'
将下载js和解析执行js分离出来。不阻止其他下载，在所有浏览器中script都是并行下载不阻止渲染直到真正需要时。

### 异步加载的问题
在异步加载的时候，无法使用document.write输出文档内容
在同步模式下，document.write是在当前script所在位置输出文档的。而在异步模式下，浏览器继续处理后续页面的内容，根本无法确定document.write应该输出到什么位置，所以异步模式下document.write不可行。而到了页面已经onload之后，再执行document.wrire将导致当前页面的内容被清空，因为它会自动触发document.open方法。实际上docuemnt.write的名声并不好，最好少用。
### jsonp
常用于跨域请求数据的JSONP，也可以理解为一种脚本的同步加载：我们在本站声明一个回调函数，然后向外站用src请求一个js文件，文件中的代码是用外站的数据对本站函数进行调用，就实现了使用外站数据的目的。虽然函数是本站声明的，数据是外站的，外站调用本站函数，但是把它理解为同步加载和执行就简单了。

	