----
title: Nginx 入门
----

#### Nginx 入门

Nginx由核心和模块组成


Gzip

Server 节点

一个server就是一个虚拟主机

### 在一个nginx上面配置多个虚拟主机的方法：

配置多个server

	server 	{
		listen 80;
		server_name xiaoyu.work;   //这里只写域名就行了不用加协议
		root index.html;
		
		location / {
			root html/CoffeeandTea.github.io;  //这是一个项目
			index index.html;
		}
	}
	
	// 配置另一个项目的地址
	server 	{
		listen 80;
		server_name stormen.cn;  // 第二个域名
		root index.html;
		
		location / {
			root html/CoffeeandTea.github.io;  //这是另一个项目
			index index.html;
		}
	}
	
	
这样配置后，重启nginx就可以做到：访问xiaoyu.work访问的是博客的项目，访问stormen.cn访问的是另一个项目了。


有的网站不支持IP访问

apache是多线程的，它为每个请求开启一个新的线程（或者是进程，取决于你的配合）。Nginx和node.js不是多线程的，因为线程的消耗太重了，所以他们采用单线程，基于事件的，这就把处理众多连接所产出的线程/进程消耗消除了。

node中的模型跟浏览器中很是类似，如果有三个人同时访问一个node站点的话，三个请求的真正处理操作不在这个node线程中，这个线程运行的是event loop，把请求分发到worker线程，worker线程应该是c++实现的，完了之后再通过callback的方式把返回的结果返回给客户端。
可以类比于浏览器中的webapi的事件。

node 运行的机制：

* V8引擎解析javascript脚本
* 解析后的代码，调用node api
* libuv库负责nodeAPI的执行，它将不同的任务分配给不同的线程，形成一个event loop（事件循环），以异步的方式将任务的执行结果返回给V8引擎。
* V8引擎再将结果返回给用户。





