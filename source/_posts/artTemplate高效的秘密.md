----
title: artTemplate高效的秘密
----

## artTemplate高效的秘密
### 预编译
arttemplate的编译赋值过程是在渲染之前完成的，这种方式称之为‘预编译’。artTemplate模板编译器会根据一些简单的规则提取好所有模板变量，声明在渲染函数头部，这个函数类似下面这样：	

	var render = function($data){
		var content = $data.content,
		$out = '';
		$out += '<h3>';
		if(typeof content === 'string'){
				$out += content;
		}
		$out += '<h3>';
		return $out;
	}
这个自动生成的函数就如同一个手工编写的JavaScript函数一样，同等的执行次数下无论CPU还是内存占用有有显著减少，性能近乎极限。
### 更快的字符串相加方式
很多人误以为数组 push 方法拼接字符串会比 += 快，要知道这仅仅是 IE6-8 的浏览器下。实测表明现代浏览器使用 += 会比数组 push 方法快，而在 v8 引擎中，使用 += 方式比数组拼接快 4.7 倍。所以 artTemplate 根据 javascript 引擎特性采用了两种不同的字符串拼接方式。