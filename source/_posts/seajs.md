----
title: sea.js
----

### seajs
在seajs中，所有的JavaScript模块都遵循CMD模块定义规范，该规范明确了模块的基本书写格式和基本交互规则。在CMD规范中一个模块就是一个文件。代码的书写格式如下：

	define(factory);
	
#### define Function
define是一个全局函数，用来定义模块。
define接受factory参数，factory可以是一个函数，也可以是一个对象或者字符串。factory为对象、字符串时，表示模块的接口就是该对象、字符串。比如可以如下定义一个json数据模块：

	define（{“foo”：“bar”}）;